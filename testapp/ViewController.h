//
//  ViewController.h
//  testapp
//
//  Created by YING CHEN on 2014-06-15.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *usernameText;
    IBOutlet UITextField *passwordText;
    IBOutlet UIButton *loginButton;
    IBOutlet UILabel *password;
    IBOutlet UILabel *userName;
}


@property (retain, nonatomic) IBOutlet UILabel *userName;

@property (retain, nonatomic) IBOutlet UITextField *usernameText;
@property (retain, nonatomic) IBOutlet UILabel *password;

@property (retain, nonatomic) IBOutlet UITextField *passwordText;
@property (retain, nonatomic) IBOutlet UIButton *loginButton;

-(IBAction)buttonClick:(id)sender;

@end
